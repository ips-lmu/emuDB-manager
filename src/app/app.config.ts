export const appConfig = {
	enableLoginForm: false,
	urls: {
		emuWebApp: 'https://ips-lmu.github.io/EMU-webApp/',
		managerAPIBackend: 'https://www.phonetik.uni-muenchen.de/apps/emuDB-manager/server-side/emudb-manager.php',
		nodeJSServer: 'wss://webapp2.phonetik.uni-muenchen.de:17890/manager'
	}
};
