# 1.2.1 (2017-04-20)

- Bugfix: Pass secret token to backend when downloading a db

# 1.2.0 (2017-04-13)

- Login mechanism changed from project accounts to individual accounts

# 1.1.0 (2017-01-18)

- Bundle list generator now creates shuffled bundle lists per default (#3)
- Bundle list generator now offers preview of what the given regular 
  expressions would do
- Uploaded databases can now be merged into an existing database (#2)

# 1.0.0 (2017-01-16)

- After a few months in production, bumped version number to 1.0.0